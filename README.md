# mpa-datasets

metalab package archive (MPA) repository for datasets

Fork of https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo

Lists of packages:
- https://sat-mtl.gitlab.io/distribution/mpa-datasets/debs/dists/sat-metalab/main/binary-arm64/Packages
- https://sat-mtl.gitlab.io/distribution/mpa-datasets/debs/dists/sat-metalab/main/binary-amd64/Packages

## Installation

In a terminal:

```sh
sudo apt install -y coreutils wget && \
wget -qO - https://sat-mtl.gitlab.io/distribution/mpa-datasets/sat-metalab-mpa-keyring.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg && \
echo 'deb [ signed-by=/usr/share/keyrings/sat-metalab-mpa-keyring.gpg ] https://sat-mtl.gitlab.io/distribution/mpa-datasets/debs/ sat-metalab main' \
    | sudo tee /etc/apt/sources.list.d/sat-metalab-mpa-datasets.list && \
sudo apt update
```
